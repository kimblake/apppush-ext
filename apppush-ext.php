<?php
/**
 * Plugin Name:     Apppush Ext
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     apppush-ext
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Apppush_Ext
 */
if(class_exists('AppPresser_Notifications')):
  PUSH_EXT_VER = 0.1;
  APPPUSH_EXT_DIR = plugin_dir_path( __FILE__ );
  
  // modify appppush js by coping into extended, regristed the modified version.
  function fancy_scripts(){
  
    // diabled original
    wp_deregister_script( 'appp_push_init' );
    // adding modified version
    wp_enqueue_script( 'appp_push_init', 
      $APPPUSH_EXT_DIR .'/src/apppush/js/appp-push-init.js',
        ['appp_cordova_push', 'appp_pushwoosh'], $PUSH_EXT_VER, true );
    // registering modified version
    wp_register_script( 'appp_push_init' );
    
    
  }

endif;